FROM openjdk:8-jdk

ARG SCRIPTS=/scripts

ARG ANDROID_COMPILE_SDK="29"
ARG ANDROID_BUILD_TOOLS="29.0.3"
ARG ANDROID_SDK_TOOLS_REV="3859397"

ENV ANDROID_COMPILE_SDK=${ANDROID_COMPILE_SDK}
ENV ANDROID_BUILD_TOOLS=${ANDROID_BUILD_TOOLS}
ENV ANDROID_SDK_TOOLS_REV=${ANDROID_SDK_TOOLS_REV}

ENV SCRIPTS=${SCRIPTS}
ENV PATH="$PATH:"${SCRIPTS}

ENV ANDROID_HOME=/android-sdk
ENV PATH="$ANDROID_HOME/tools:$ANDROID_HOME/tools/bin:$ANDROID_HOME/platform-tools:$PATH"

RUN mkdir ${SCRIPTS}
COPY ./android-pre-conf ${SCRIPTS}/android-pre-conf

RUN mkdir ${ANDROID_HOME} \
    && wget --quiet --output-document=${ANDROID_HOME}/android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS_REV}.zip \
    && unzip -o -qq ${ANDROID_HOME}/android-sdk.zip -d ${ANDROID_HOME} \
    && rm ${ANDROID_HOME}/android-sdk.zip \
    && yes | sdkmanager "platform-tools" \
    && yes | sdkmanager "platforms;android-"${ANDROID_COMPILE_SDK} \
    && yes | sdkmanager "build-tools;"${ANDROID_BUILD_TOOLS} \
    && yes | sdkmanager "extras;android;m2repository" \
    && yes | sdkmanager "extras;google;m2repository" \
    && yes | sdkmanager "extras;google;instantapps" \
    && yes | sdkmanager "ndk-bundle" \
    && yes | sdkmanager --licenses > /dev/null \
    && chmod a+x ${SCRIPTS}/android-pre-conf